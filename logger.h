#ifndef LOGGER_H_
#define LOGGER_H_

#ifdef __cplusplus
extern "C" {
#endif


#ifndef LOGGER_TRANSMIT_TIMEOUT
#define LOGGER_TRANSMIT_TIMEOUT 1000
#endif

#define LOGGER_OUPTUT_BUFFER_SIZE 512

/** Custom printf with timestamp `[timestamp] ...`
 * @param *fmt String to print
 * @param ... Data
 */
void simple_log(const char *fmt, ...);

/** Custom printf function, only translate to va_list arg HAL_UART.
 * @param *fmt String to print
 * @param ... Data
 */
void simple_printf(const char *fmt, ...);

/**
 * Simple Logger init.
 * @param *uart, UART channel to communicate with
 * @param timeout, timeout for message. If it takes too long and there is another message, the first one will stop and the second one will be sent
 */
void simple_logger_init(void * uart, uint32_t timeout);

/**
 * Handle finish of sending
 * @param *uart, UART channel to communicate with
 */
void simple_logger_complete_tx(void * uart);

#ifdef __cplusplus
}
#endif
#endif /* LOGGER_H_ */
