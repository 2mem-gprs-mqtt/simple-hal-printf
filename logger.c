#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include "stm32g0xx_hal.h"
#include "logger.h"


#ifdef MQTT_TASK
#include "cmsis_os.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"
#define GETTICK osKernelSysTick
osMutexDef(loggerMutex);
static osMutexId loggerMutexHandle;

#else
#define GETTICK HAL_GetTick
#endif



UART_HandleTypeDef * printf_uart = 0;
static volatile bool dmaIsBusy = false;
static uint32_t HAL_LOGGER_MAX_DELAY = 0xFFFFFFFF;
static uint32_t lastTick = 0;



/** Custom printf function, only translate to va_list arg HAL_UART.
 * @param *fmt String to print
 * @param ... Data
 */
void simple_printf(const char *fmt, ...) {
	if (!printf_uart) return;

#ifdef MQTT_TASK
	if (osMutexWait(loggerMutexHandle, portMAX_DELAY ) == osOK) {
#endif
	va_list argp;

	char string[LOGGER_OUPTUT_BUFFER_SIZE];

	va_start(argp, fmt);
	if (vsnprintf(string,LOGGER_OUPTUT_BUFFER_SIZE, fmt, argp) > 0){
		HAL_UART_Transmit(printf_uart, (uint8_t*)string, strlen(string), 1000); // send message via UART
	} else {
		HAL_UART_Transmit(printf_uart, (uint8_t*)"E - Print\n", 11, 1000);
	}
	//simple_printf_valist(fmt, argp);
	va_end(argp);
#ifdef MQTT_TASK
	osMutexRelease(loggerMutexHandle);
	}
#endif
}

void simple_logger_init(void * uart, uint32_t timeout) {
	printf_uart = (UART_HandleTypeDef *)uart;
	HAL_LOGGER_MAX_DELAY = timeout;
	lastTick = GETTICK();
#ifdef MQTT_TASK
	loggerMutexHandle = osMutexCreate(osMutex(loggerMutex));
#endif
}

void simple_logger_complete_tx(void * uart){
	if (uart != printf_uart ) return;
	dmaIsBusy = false;
}

void simple_log(const char *fmt, ...){
	if (!printf_uart) return;
#ifdef MQTT_TASK
	if (osMutexWait(loggerMutexHandle, portMAX_DELAY ) == osOK) {
#endif
	va_list argp;


	static char a[LOGGER_OUPTUT_BUFFER_SIZE];
	static char string[LOGGER_OUPTUT_BUFFER_SIZE];

	va_start(argp, fmt);
	if (vsprintf(string, fmt, argp) > 0){
		snprintf(a, LOGGER_OUPTUT_BUFFER_SIZE, "[%6d]: %s", (int)GETTICK(), string);
		HAL_UART_Transmit(printf_uart, (uint8_t*)a, strlen(a), 1000); // send message via UART
	} else {
		HAL_UART_Transmit(printf_uart, (uint8_t*)"E - Print\n", 11, 1000);
	}
	//simple_printf_valist(fmt, argp);
	va_end(argp);
#ifdef MQTT_TASK
	osMutexRelease(loggerMutexHandle);
	}
#endif

}


